/* globals $ */

$('#retriever').submit(function(e) {
  e.preventDefault();
});

$('#retriever').keyup(debounce(200, function(e) {
  e.preventDefault();

  if (e.keyCode === 27) { //escape
    $('body').removeClass('retriever-open');
    $('#retriever-results').html('');
    $('#retriever-search').blur();
  } else {
    $.ajax({
      'type': 'post',
      'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
      'data': $('#retriever').serialize(),
      'url': retriever.postUrl,
    }).done(function (response) {
      if (document.retriever.retrieverSearch.value.length < 1) {
        $('#retriever-results').html('');
      } else if (response.length < 1) {
        $('#retriever-results').html('').append('No results found');
      } else {
        $('#retriever-results').html(response);
      }
    }).fail(function(error) {
      $('#retriever-results').html('').append('An error has occured');
    });
  }
}));

$('.retriever-trigger').click(function() {
  ($('body').hasClass('retriever-open') ? $('#retriever-search').blur() : $('#retriever-search').focus());

  if ($('body').hasClass('retriever-open') && $(this).hasClass('retriever')) {
    $('#retriever-search').focus();
  } else {
    $('body').toggleClass('retriever-open');
    $('#retriever-results').html('');
    $('#retriever-search').val('');
  }
});


// Debounce
// ==========
function debounce(delay, callback) {
  var timeout = null;

  return function () {
    if (timeout) {
      clearTimeout(timeout);
    }

    var args = arguments;

    timeout = setTimeout(function () {
      callback.apply(null, args);
      timeout = null;
    }, delay);
  };
}
