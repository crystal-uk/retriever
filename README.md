# ![](resources/icon-small.png) Retriever plugin for Craft CMS

Dynamically search CraftCMS entries

## Installation

1. Run composer require `crystal-uk/postal`
2. Install Postal within Craft by navigating to /settings/plugins and clicking `install`

## Usage

* Add `{% hook 'retriever' %}` wherever the form should appear.
* Add the ID `retriever-results` wherever the search results should be appended
* Add the class `retriever-trigger` to the element that toggles the form


Brought to you by [Crystal](http://wearecrystal.uk)
