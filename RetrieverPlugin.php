<?php
namespace Craft;

class RetrieverPlugin extends BasePlugin {
  function getName() {
    return Craft::t('Retriever');
  }

  function getVersion() {
    return '1.0.25';
  }

  function getDeveloper() {
    return 'Crystal';
  }

  function getDeveloperUrl() {
    return 'http://wearecrystal.uk';
  }

  public function getDescription() {
    return Craft::t('Dynamically search CraftCMS entries');
  }

  public function getSettingsHtml() {
    return craft()->templates->render('Retriever/settings', array(
      'settings' => $this->getSettings()
    ));
  }

  protected function defineSettings() {
    return array(
      'sections' => array(AttributeType::Mixed, 'default' => array()),
      'templatePath' => array(AttributeType::String)
    );
  }

  public function init() {
    craft()->templates->hook('retriever', function() {
      // by default Craft won't look in the plugin for templates
      // this is necassary to stop Craft looking in /craft/templates
      // https://craftcms.com/docs/plugins/templates

      $oldPath = craft()->path->getTemplatesPath();
      $newPath = craft()->path->getPluginsPath(). 'retriever/templates'; 
      craft()->path->setTemplatesPath($newPath);

      $html = craft()->templates->render('retrieverForm');

      craft()->path->setTemplatesPath($oldPath); // reset the template path !important
      
      return $html;
    });
  }
}
