<?php
/**
 * @author    Crystal
 * @copyright Copyright (c) 2017 Crystal
 * @link      http://wearecrystal.uk
 * @package   Retriever
 * @since     1.0.0
 */

namespace Craft;

class RetrieverController extends BaseController {
  /**
   * @var    bool|array Allows anonymous access to this controller's actions.
   * @access protected
   */
  protected $allowAnonymous = array('actionIndex');

  // Handle a request going to our plugin's index action URL, e.g.: actions/retriever
  public function actionIndex() {
    $criteria = craft()->elements->getCriteria(ElementType::Entry);
    $criteria->section = craft()->plugins->getPlugin('retriever')->getSettings()->sections;
    $criteria->search = '*' . craft()->request->getPost('retrieverSearch') . '*';
    $entries = $criteria->find();

    if (!$path = craft()->plugins->getPlugin('retriever')->getSettings()->templatePath) {
      $path = 'retriever/results';
    }

    $template = craft()->templates->render($path, ['entries' => $entries]);

    $this->returnJson($template);
  }
}
